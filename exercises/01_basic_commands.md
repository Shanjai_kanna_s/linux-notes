# Linux: Basic Exercises

> This section assumes you just started linux command line. Use the basic commands you know to solve the problems

> Make use of the `man` command to learn more about the functionality and the options

> Try using `man` before searching online  

## Command Set

||||||
|---|----|----|----|----|
|echo|date|pwd|sleep|clear|
|time|cal|man|whereis|which|

## Problem Set 1

1. display a text on the screen
1. display two words on screen, each word in separate lines using one command
1. clear the screen using appropriate command
1. clear the screen without using any command
1. How can we verify `sleep 5` actually took 5 seconds ? **Hint**: There are two ways to do it
1. display the current working directory
1. display the calendar of current month; with and without highighting current date
1. display the calendar of previous, current and next months 
1. find out the location of the commands you know 
1. find out whether the commands are installed in only one location or many 

-----

## Problem Set 2

1. access the documentation of the `date` command and find out how to display the month as 'Jan'. Can we make it JAN as well ?
1. display the current date as number of seconds since 1-Jan-1970 (also called epoch)
1. display the current date in the following formats; CCYY-MM-DD, DD-MMM-CCYY and DDD-CCYY. MMM=Jan, Feb,.., DDD=Days passed since start of the year
1. can you display CCYY-MM-DD with one format specifier (%?) ?
1. can you display DD-MMM-YYYY with one format specifier (%?) ?
1. display date as CC-YY-MM-DD
1. display time in HH:MM:SS [AM or PM] format 
1. display time in HH:MM:SS TZ format (TZ = Timezone)
1. display time in HH:MM:SS with one format specifier (%?) ?
1. display date and time as `Dyyyymmdd_hhmmss` format. This can be used later to create file names with current timestamp

----

## Problem Set 3

1. display a triangle of 5 line deep using  "\*". 
Example: 
```
*
**
***
****
*****
```
1. display a triangle 5 line deep using  "\*", right justify the vertical line.
1. Here are som builtin shell variables. display these variables and understand the contents. 
- $USER
- $SHELL
- $BASH_VERSION
- $RANDOM
- $PWD
- $OLDPWD
- $PATH
1. In the above variables, does any of these change value when we display it multiple times ?
